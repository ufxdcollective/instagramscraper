<?php


namespace UFXDCollective\InstagramScraper;


use GuzzleHttp\Client;
use Sunra\PhpSimple\HtmlDomParser;

class Scraper
{

    protected $user;
    protected $http;

    protected $cache_dir = '';
    protected $cache_enabled = false;
    protected $cache_timeout = 0;

    public function __construct($user, $cache_dir = NULL, $cache_timeout = 0)
    {
        $this->user = $user;
        $this->http = new Client([
            'headers' => [
                'accept-encoding' => 'gzip, deflate',
                'accept-language' => 'en-US,en;q=0.9,sv;q=0.8,la;q=0.7',
                'cache-control' => 'no-cache',
                'pragma' => 'no-cache',
                'sec-fetch-mode' => 'cors',
                'sec-fetch-site' => 'cross-site',
                'user-agent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.79 Safari/537.36'
            ]
        ]);

        if($cache_dir){
            $this->enableCache($cache_dir, $cache_timeout);
        }

    }


    public function enableCache($cache_dir, $timeout = 0){
        $this->cache_dir = $cache_dir;
        $this->cache_timeout = $timeout;

        if(!is_dir($this->cache_dir)){
            mkdir($this->cache_dir, 0777, true);
        }
        $this->cache_enabled = true;
    }


    public function disableCache(){
        $this->cache_enabled = false;
    }


    public function cacheKey($key){
        return $this->user . '-' . $key;
    }


    public function cacheFile($key){
        return $this->cache_dir . '/' . $this->cacheKey($key);
    }


    public function cacheGet($key, $default = NULL, $timeout = 0){

        $cache_path = $this->cacheFile($key);
        if( $this->cacheExists( $key, $timeout ) ){
            return unserialize( file_get_contents($cache_path) );
        }

        if(is_callable($default)){
            try {
                $result = call_user_func($default, $key);
                $this->cacheSet($key, $result);
                return $result;
            }catch (\Exception $e){}

            return NULL;
        }

        return $default;

    }


    public function cacheExists($key, $timeout = 0){

        $cache_path = $this->cacheFile($key);

        if( file_exists( $cache_path ) ){

            if(!$timeout OR $timeout <= 0){
                return true;
            }

            $now = time();
            $file_time = filemtime($cache_path);
            if( $now - $file_time < $timeout ){
                return true;
            }

        }

        return false;

    }


    public function cacheSet($key, $data){
        $cache_path = $this->cacheFile($key);
        file_put_contents( $cache_path, serialize($data) );
    }


    protected function _latestPost(){

        $url = "https://www.instagram.com/$this->user/";

        $response = $this->http->get($url);
        $html = $response->getBody()->getContents();
        preg_match("/window\._sharedData = {(.*)};<\/script>/i", $html, $matches);

        $json = '{' . $matches[1] . "}";

        $data = json_decode($json, true);
        $node = $data['entry_data']['ProfilePage'][0]['graphql']['user']['edge_owner_to_timeline_media']['edges'][0]['node'] ?? NULL;

        if(!$node) throw new \Exception('Node not found');

        $image = $node['thumbnail_src'] ?? NULL;
        if(!$image)  throw new \Exception('Image not found');

        $caption = $node['edge_media_to_caption']['edges'][0]['node']['text'] ?? NULL;

	$link = "https://www.instagram.com/p/" . $node['shortcode'];

        return [
            'image' => $image,
            'caption' => $caption,
            'link' => $link,
        ];
    }


    public function latestPost(){

        if( $this->cache_enabled ){

            return $this->cacheGet('latest-posts', function(){

                $data = $this->_latestPost();

                $cache_image_key = 'cached_image.jpg';
                $cached_image = $this->cacheKey($cache_image_key);
                $cache_path = $this->cacheFile($cache_image_key);

                $this->http->get($data['image'], [
                    'sink' => $cache_path
                ]);

                $data['cached_image'] = $cached_image;

                return $data;

        }, $this->cache_timeout);


        }else{
            try {
                return $this->_latestPost();
            }catch (\Exception $e){}
        }

        return NULL;

    }

}
