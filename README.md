# Scrape Instagram Public Profile

## Usage

```php

use UFXDCollective\InstagramScraper\Scraper;

$cache_dir = __DIR__ . '/cache';
$cache_timeout = 60 * 60 * 24; // 1 day in seconds

$scraper = new Scraper($cache_dir);

// Enable Cache.
$scraper->enableCache($cache_dir, $cache_timeout);

// Get the latest post.
$data = $scraper->latestPost();


// Structure of the $data variable will be:
$data = [
    'image' => 'url/of/image/',
    'cached_image' => 'cached_filename',
    'caption' => 'Image Caption',
];

```

